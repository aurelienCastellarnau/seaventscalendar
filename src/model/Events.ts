/*
 * File: events.ts
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 10:10:46 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:33:06 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { isString, isDate, isBoolean } from "util"

 // a random colors set
 // [wemanity color set]
const colors = [
    "#00aced",
    "#f41397",
    "#fff000",
    "#ed3c23",
    "#f61588",
    "#00b4ac",
    "#2f4b5d",
    "#e84d0e",
    "#7b5c9d",
]

// FullCalendarEvent interface is a copycat of Fullcalendar event expected properties
export interface FullCalendarEvent {
    id?: number
    title: string
    dateStr: string
    date: Date
    start: Date
    end: Date
    allDay: boolean
    backgroundColor?: string
    resource?: any
    dayEl?: HTMLElement
    jsEvent?: MouseEvent
    view?: any
}
// shortcut for possibles data types in Events
export type T_Events_Props = number | string | boolean | Date

// Events class, work with seavents API and FullCalendar.io
export class Events implements FullCalendarEvent {
    id?: number
    // exposed event name
    title: string
    // fc property
    dateStr: string
    allDay: boolean
    // if date is set but not end it will be a 'one day' event
    date: Date
    // start and end allow to display several days event
    start: Date
    end: Date
    // will allow to manage parent/child events
    events: Events[]
    // randomly set for now
    backgroundColor?: string
    // childs getter/setter
    getEvents: () => Events[]
    setEvents: (events: Events[]) => void
    // hydrator/accessor
    val: (key: string, val?: T_Events_Props | undefined) => T_Events_Props

    // this constructor pattern allow to instanciate like new Events(), new Events({}) or new Events({title: 'only'})...
    constructor(hydrator: FullCalendarEvent = {} as FullCalendarEvent) {
        let {
            id = 0,
            title = "",
            dateStr = "",
            allDay = false,
            date = new Date(),
            start = new Date(),
            end = new Date(),
        } = hydrator;

        this.id = id
        this.title = title
        this.dateStr = dateStr
        this.allDay = allDay
        this.date = typeof date === 'string' ? new Date(date) : date
        this.start = typeof start === 'string' ? new Date(start) : start
        this.end = typeof end === 'string' ? new Date(end) : end
        this.events = new Array<Events>()
        this.backgroundColor = colors[Math.floor(Math.random() * colors.length)];

        // we bind private definitions to this and we expose methods in public properties
        this.val = this.valDefinition.bind(this)
        this.getEvents = this.getEventsDefinition.bind(this)
        this.setEvents = this.setEventsDefinition.bind(this)
    }

    // hydrator/accessor method
    private valDefinition = (key: string, val: T_Events_Props | undefined = undefined): T_Events_Props => {
        if (val === undefined) {
            switch (key) {
                case "id":
                    return this.id !== undefined ? this.id : 0
                case "title":
                    return this.title
                case "date":
                    return this.date
                case "dateStr":
                    return this.dateStr
                case "start":
                    return this.start
                case "end":
                    return this.end
                case "allDay":
                    return this.allDay
                case "backgroundColor":
                    return this.backgroundColor !== undefined ? this.backgroundColor : "#AA9F39"
                default:
                    return false
            }
        } else {
            switch (key) {
                case "title":
                    this.title = isString(val) ? val : this.title
                    return this.title
                case "date":
                    // We align start on date input
                    let date = isString(val) ? new Date(val) : val
                    this.date = isDate(date) ? date : this.date
                    if (this.start !== this.date) {
                        // avoid ref on date obj
                        this.start = new Date(this.date.getTime())
                    }
                    return this.date
                case "dateStr":
                    this.dateStr = isString(val) ? val : this.dateStr
                    return this.dateStr
                case "start":
                    // We align date on start input
                    let start = isString(val) ? new Date(val) : val
                    this.start = isDate(start) ? start : this.start
                    if (this.start !== this.date) {
                        // avoid ref on date obj
                        this.date = new Date(this.start.getTime())
                    }
                    return this.start
                case "end":
                    let end = isString(val) ? new Date(val) : val
                    this.end = isDate(end) ? end : this.end
                    return this.end
                case "allDay":
                    this.allDay = isBoolean(val) ? val : this.allDay
                    return this.allDay
                case "backgroundColor":
                    this.backgroundColor = isString(val) ? val : this.backgroundColor
                    return this.backgroundColor !== undefined ? this.backgroundColor : ""
                default:
                    return false
            }
        }
    }


    private getEventsDefinition = () => {
        return this.events
    }
    private setEventsDefinition = (events: Events[]) => {
        this.events = events
    }
}

// Type guards
export const IsEvents = (item: any): item is Events => {
    const ie = item !== null && 'undefined' !== typeof item &&
    'undefined' !== typeof item.id && 'number' == typeof item.id &&
    'undefined' !== typeof item.title && 'string' == typeof item.title &&
    'undefined' !== typeof item.start && 'undefined' !== typeof item.end &&
    'undefined' !== typeof item.date && 'undefined' !== typeof item.allDay && 'boolean' == typeof item.allDay;
    return ie
}

export const IsEventsInputCompatible = (item: any): item is Events => {
    return item !== null && 'undefined' !== typeof item &&
    'undefined' !== typeof item.title && 'string' == typeof item.title &&
    'undefined' !== typeof item.date && 'object' == typeof item.date &&
    'undefined' !== typeof item.start && 'object' == typeof item.start &&
    'undefined' !== typeof item.end && 'object' == typeof item.end &&
    'undefined' !== typeof item.allDay
}
