/*
 * File: UserFormError.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 1:32:26 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 2:58:27 am
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { isBoolean, isDate } from "util"
import { T_Events_Props } from "./Events"

// WIP EventsFormError help us to manage
// Events integrity and user feedback in react/material forms
// the state will maintain errors and reducer will populate error
// throught pure calls on
export class EventsFormError {
    title: string
    dateStr: string
    allDay: string
    date: string
    start: string
    end: string
    backgroundColor: string
    has: boolean
    get: (key: string) => string | undefined

    constructor(hydrator: EventsFormError = {} as EventsFormError){

        let {
            title = "",
            dateStr = "",
            allDay = "",
            date = "",
            start = "",
            end = "",
            backgroundColor = ""
        } = hydrator

        this.title = title
        this.dateStr = dateStr
        this.allDay = allDay
        this.date = date
        this.start = start
        this.end = end
        this.backgroundColor = backgroundColor
        this.has = this.hasError()

        this.get = this.getDefinition.bind(this)
    };

    // private definition for get()
    private getDefinition = (key: string): string | undefined => {
        switch (key) {
            case "title":
                return this.title
            case "date":
                return this.date
            case "dateStr":
                return this.dateStr
            case "start":
                return this.start
            case "end":
                return this.end
            case "allDay":
                return this.allDay
            case "backgroundColor":
                return this.backgroundColor
        }
    }

    // check must receive data in readonly and return undefined | good error message
    public check = (propname: string, value: T_Events_Props) => {
        const isType = (type: string) => (value: any) => typeof value == type ? undefined : 'Bad type provided'
        const required = (value: T_Events_Props)  => value ? undefined : 'Required'

        switch(propname) {
            case "title":
                if (!isBoolean(value)) {
                    return isType("string")(value) && required(value);
                }
                return isType("string")(value)
            case "date":
                if (isDate(value))
                    return required(value);
                break;
            default:
                break;
        }
    }

    // hasError check if a property is fullfilled
    hasError = (): boolean => {
        return (this.title !== null && this.title !== "") ? true : false ||
            (this.date !== null && this.date !== "") ? true : false
    }
}
