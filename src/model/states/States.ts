/*
 * File: ContainerState.ts
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 3:41:49 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 3:01:19 am
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */
import { Events } from '../Events'
import { SeaventsError } from '../SeaventsError'
import FullCalendar from '@fullcalendar/react'
import { EventInput, OptionsInput } from '@fullcalendar/core'
import { EventsFormError } from '../EventsFormError'

// CalendarState maintain functionnal aspects
// of FullCalendar Component
export interface CalendarState {
    events: EventInput[]
    opts: OptionsInput
    ref: React.RefObject<FullCalendar> | null
}

// EventsState maintain our model
// This state has a functionnal aspect through
// selectedEvents and newEvents wich maintains user
// interactions with model
export interface EventsState {
    loading: boolean
    selectedEvents: Events
    newEvents: Events
    errors: EventsFormError
    events: Events[]
    error: SeaventsError
}

// ContainerState
export interface ContainerState {
    title: string
}

export interface SeaventsState {
    Container: ContainerState
    Calendar: CalendarState
    Events: EventsState
}