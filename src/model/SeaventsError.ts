/*
 * File: SeaventsError.ts
 * Project: Seavents
 * File Created: Friday, 20th September 2019 3:21:33 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 6:20:50 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

export interface SeaventsError {
    code: number
    message: string
}

// type guard
export const isSeaventsError = (item: any): item is SeaventsError => {
    return item != null && typeof item != 'undefined' &&
    typeof item.code != 'undefined' && item.code != null &&
    typeof item.message != 'undefined' && item.message != null;
}
