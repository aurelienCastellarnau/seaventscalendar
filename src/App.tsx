import React from 'react'
import './App.css'
import Container from './components/container/Container'
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const App: React.FC = () => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <div className="App">
        <Container ></Container>
      </div>
    </MuiPickersUtilsProvider>
  )
}

export default App
