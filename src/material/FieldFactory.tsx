/*
 * File: FieldFactory.tsx
 * Project: Seavents
 * File Created: Sunday, 22nd September 2019 10:42:12 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:42:02 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import { IFormStyle } from './model/IFormStyle';
import classNames from 'classnames';
import { T_Events_Props } from '../model/Events';
import { EventsFormError } from '../model/EventsFormError';
import { isDate } from 'util';
import { DateTimePicker, MaterialUiPickersDate } from '@material-ui/pickers';
import { Checkbox, FormControlLabel } from '@material-ui/core';

export type EventFormFields = {
    id: string
    name: string
    value: T_Events_Props
    label: string
    type: string
    onChange: (key: string, value: T_Events_Props) => void
    variant: any
    render: (field: EventFormFields, classes: IFormStyle, errors: EventsFormError) => void
}

// TextField
export const renderTextField = (field: EventFormFields, classes: IFormStyle, errors: EventsFormError) => {
    const onChange = (e: any) => {
        field.onChange(e.target.name, e.target.value)
    }
    return (
        <TextField
            key={field.id}
            id={field.id}
            name={field.name}
            variant={field.variant}
            className={classNames(classes.textfield)}
            label={field.label}
            value={field.value}
            error={errors.get(field.name) !== undefined && errors.get(field.name) !== ""}
            helperText={errors.get(field.name)}
            onChange={onChange}
            type={field.type}
        />
    );
}

// DateTimePicker
export const renderDateField = (field: EventFormFields, classes: IFormStyle, errors: EventsFormError) => {
    if (isDate(field.value)) {
        const onChange = (date: MaterialUiPickersDate) => {
            if (date !== null) {
                field.onChange(field.name, date)
            }
        }
        return (
          <DateTimePicker
            key={field.id}
            id={field.id}
            name={field.name}
            value={field.value}
            onChange={onChange}
            label={field.name}
          />
        );
    } else {
        return (
            <p key={field.id}>invalid date format</p>
        )
    }
}

// Checkbox in FormControlLabel
export const renderCheckboxField = (field: EventFormFields, classes: IFormStyle, errors: EventsFormError) => {
    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        field.onChange(field.name, e.target.checked)
    }
    return (
        <div key={field.id}>
            <FormControlLabel
                value={field.value}
                control={
                    <Checkbox
                        key={field.id}
                        className={classNames(classes.checkbox)}
                        id={field.id}
                        checked={field.value !== false}
                        value={field.value}
                        onChange={onChange}
                        color="primary"
                        inputProps={{
                            'aria-label': field.label,
                        }}
                    />
                }
                label={field.name}
                labelPlacement="start"
            />
        </div>
    )
}