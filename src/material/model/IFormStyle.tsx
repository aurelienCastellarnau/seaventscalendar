/*
 * File: IFormStyle.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 1:15:52 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 12:36:47 am
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

export interface IFormStyle {
    form: {}
    checkbox: {}
    fieldsContainer: {}
    textfield: {}
    input: {}
    button: {}
}

export type IFormStyler = (Theme: any) => IFormStyle
