/*
 * File: IFormEntity.tsx
 * Project: Seavents
 * File Created: Sunday, 22nd September 2019 10:42:56 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd September 2019 10:45:39 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

interface IFormEntity {
    propToLabel: (property: string) => string;
}

export default IFormEntity;
