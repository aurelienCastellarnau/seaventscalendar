/*
 * File: theme.ts
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 4:07:17 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 8:01:47 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { createMuiTheme, Theme as MaterialTheme } from '@material-ui/core/styles'

const Theme: MaterialTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#2f4b5d"
    },
    secondary: {
      main: "#00b4ac"
    },
    error: {
      main: "#f24223"
    },
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    // type: "dark",
  },
  typography: {
      // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
})

export default Theme
