/*
 * File: Container.tsx
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 3:33:25 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 8:12:17 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
import { ContainerState, SeaventsState } from '../../model/states/States'
import { ThunkDispatch } from 'redux-thunk'
import { connect } from 'react-redux'
import FcContainer from './FcContainer'
import { FetchEvents } from '../../store/actions/events/get'

type ContainerProps = {
    data: SeaventsState
    action: {
        fetchEvents: () => void
    }
  }

export class Container extends React.Component<ContainerProps, ContainerState> {
    componentDidMount() {
        this.props.action.fetchEvents()
    }
    render () {
        return (
            <FcContainer data={this.props.data}></FcContainer>
        )
    }
}

const mapStateToProps = (state: SeaventsState) => ({ data: state })

const mapDispatchToProps = (dispatch: ThunkDispatch<Promise<void>, ContainerState, any>) => {
    return {
        action: {
            fetchEvents: () => {
                dispatch<any>(FetchEvents())
            },
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Container)
