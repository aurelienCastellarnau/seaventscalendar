/*
 * File: DashboardStyle.tsx
 * Project: Seavents
 * File Created: Sunday, 22nd September 2019 11:13:09 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Monday, 23rd September 2019 2:17:48 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */
import { Theme as MaterialTheme } from '@material-ui/core/styles'

const drawerWidth = 240;
const appBarHeight = 20;

const ContainerStyles = (Theme: MaterialTheme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 0,
        backgroundColor: Theme.palette.background.default,
        padding: Theme.spacing(3),
        marginTop: `calc(5% + ${appBarHeight}px)`,
    },
    // toolbar: Theme.mixins.toolbar,
});

export default ContainerStyles;