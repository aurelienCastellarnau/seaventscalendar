/*
 * File: FcContainer.tsx
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 3:33:25 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 4:35:59 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
// import PropTypes from 'prop-types'
import { SeaventsState } from '../../model/states/States'
import { Grid } from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'
import ContainerStyles from './ContainerStyle'
import ShowEvents from '../showEvents/ShowEvents'

type ContainerProps = {
    data: SeaventsState
  }

// Stateless/presentationnal component
const FcContainer: React.FC<ContainerProps> = (props: ContainerProps) => {
    // injected to props by wishStyles
    const {data} = props
    const title = () => <h1 className="title">{data.Container.title}</h1>
    return (
      <Grid container spacing={10}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          {title()}
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <ShowEvents></ShowEvents>
        </Grid>
      </Grid>
    )
}

export default withStyles(ContainerStyles)(FcContainer)
