/*
 * File: App.test.tsx
 * Project: Seavents
 * File Created: Monday, 16th September 2019 10:01:49 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 8:14:12 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import React from 'react'
import { shallow } from 'enzyme'

import { Container } from './Container'
import FcContainer from './FcContainer'
import { Events } from '../../model/Events'
import FullCalendar from '@fullcalendar/react'
import { EventsFormError } from '../../model/EventsFormError'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store';
import { Store, AnyAction } from 'redux'
import { CalendarState } from '@fullcalendar/core'
import { ContainerState, EventsState, SeaventsState } from '../../model/states/States'

const mockStore = configureStore([]);

const mockfn = jest.fn()
const mockState = {
  Container: {
    title: "SEAVENTS"
  },
  Events: {
    selectedEvents: new Events(),
    newEvents: new Events(),
    errors: new EventsFormError(),
    loading: false,
    events: new Array<Events>(),
    error: {code: 0, message: "no error"}
  },
  Calendar: {
    opts: {},
    ref: React.createRef<FullCalendar>(),
    events: [ // initial event data
        { title: 'Event Now', start: new Date() }
    ],
  }
}
let store = mockStore(mockState) as Store<{
                                            Calendar: CalendarState;
                                            Container: ContainerState;
                                            Events: EventsState;
                                          }, AnyAction> & {
                                              dispatch: unknown;
                                            }


beforeEach(() => {
  store = mockStore(mockState) as Store<{
                                          Calendar: CalendarState;
                                          Container: ContainerState;
                                          Events: EventsState;
                                        }, AnyAction> & {
                                            dispatch: unknown;
                                          }
})

test('Container Store has a title property loaded with SEAVENTS', () => {
  expect(store.getState().Container.title).toBe('SEAVENTS')
})

test('Connected Container display', () => {
  const container = shallow(
    <Provider store={store}>
      <Container></Container>)
    </Provider>
    )
})

test('Functionnal Container display the title into the h1', () => {
  const fccontainer = shallow(<FcContainer data={mockState}/>)
  const title = fccontainer.find('.title')
  expect(title.text()).toBe("SEAVENTS")
  // Snapshot
  expect(fccontainer.html()).toMatchSnapshot()
})
