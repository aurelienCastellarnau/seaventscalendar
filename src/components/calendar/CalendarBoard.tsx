/*
 * File: calendar.ts
 * Project: Seavents
 * File Created: Thursday, 19th September 2019 5:52:17 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:22:24 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
// import PropTypes from 'prop-types'
import { CalendarState, SeaventsState, EventsState } from '../../model/states/States'
import { ThunkDispatch } from 'redux-thunk'
import { connect } from 'react-redux'
import FcCalendarBoard from './FcCalendarBoard'
import { ToggleCalendarWeekends, HandleDateClick, SetOptions } from '../../store/actions/calendar'
import { OptionsInput, View } from '@fullcalendar/core'
import { Events } from '../../model/Events'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction' // needed for dayClick
import { FetchOneEvents, FetchEvents } from '../../store/actions/events/get'

export interface CalendarProps {
    data: SeaventsState,
    action: {
        fetchEvents: () => void,
        setOptions: (opts: OptionsInput) => void,
        toggleWeekends: () => void,
        handleDateClick: (e: Events) => void,
        selectEvents: (id: any) => void,
    }
}
// CalendarBoard is a connected component merging FullCalendar and Seavent API logic
// use FcCalendarBoard as functionnal component
// https://fullcalendar.io/
export class CalendarBoard extends React.Component<CalendarProps, EventsState> {
    constructor(props: CalendarProps) {
        super(props)
        // listener when user click on calendar
        const onDateClick = (arg: {
            date: Date
            dateStr: string
            allDay: boolean
            resource?: any
            dayEl: HTMLElement
            jsEvent: MouseEvent
            view: View
        }) => {
            if (window.confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
                const event: Events = new Events({
                    title: arg.dateStr,
                    start: arg.date,
                    end: arg.date,
                    dateStr: arg.dateStr,
                    date: arg.date,
                    allDay: false,
                })
                this.props.action.handleDateClick(event)
            }
        }

        // listener when user click on an event
        const onEventClick = (e: any) => {
            this.props.action.selectEvents(e.event._def.publicId)
        }

        // OptionsInput
        this.props.action.setOptions({
            defaultView: "dayGridMonth",
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            plugins: [ dayGridPlugin, timeGridPlugin, interactionPlugin ],
            weekends: true,
            events: this.props.data.Events.events,
            dateClick: onDateClick,
            eventClick: onEventClick,
        })
    }
    render () {
        return (
            <FcCalendarBoard data={this.props.data.Calendar} action={this.props.action}></FcCalendarBoard>
        )
    }
}

const mapStateToProps = (state: SeaventsState) => {
    return {
        data: {
            Container: state.Container,
            Calendar: state.Calendar,
            Events: state.Events,
        }
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<Promise<void>, CalendarState, any>) => {
    return {
        action: {
            fetchEvents: () => {
                dispatch<any>(FetchEvents())
            },
            setOptions: (opts: OptionsInput) => {
                dispatch<any>(SetOptions(opts))
            },
            toggleWeekends: () => {
                dispatch<any>(ToggleCalendarWeekends())
            },
            handleDateClick: (e :Events) => {
                dispatch<any>(HandleDateClick(e))
            },
            selectEvents: (id: any) => {
                dispatch<any>(FetchOneEvents(id))
            },
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CalendarBoard)
