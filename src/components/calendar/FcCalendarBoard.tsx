/*
 * File: FcCalendar.ts
 * Project: Seavents
 * File Created: Thursday, 19th September 2019 5:52:37 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:22:34 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import './index.css'
import * as React from 'react'
import FullCalendar from '@fullcalendar/react'
import { OptionsInput } from '@fullcalendar/core'
import { Events } from '../../model/Events'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import { CalendarState } from '../../model/states/States'
import CalendarStyliser from './CalendarStyle'
import { withStyles } from '@material-ui/core'

export interface FcCalendarProps {
    data: CalendarState,
    classes: any,
    action: {
        fetchEvents: () => void,
        setOptions: (opts: OptionsInput) => void,
        toggleWeekends: () => void,
        handleDateClick: (e: Events) => void
    }
}

// Stateless/presentationnal component
const FcCalendarBoard: React.FC<FcCalendarProps> = (props: FcCalendarProps) => {
    const { classes, data, action } = props
    const refetchEvents = () => {
        action.fetchEvents()
    }
    return (
        <div className='seavents-fc'>
            <div className='seavents-fc-top'>
                <button onClick={ action.toggleWeekends } className={classes.button}>Toggle weekends</button>
                <button onClick={ refetchEvents } className={classes.button}>Reload planning</button>
            </div>
            <div className='seavents-fc-calendar'>
                <FullCalendar
                    ref={data.ref} {...data.opts}
                    plugins={[ dayGridPlugin, timeGridPlugin, interactionPlugin ]}
                >
                </FullCalendar>
            </div>
        </div>
    )
}

export default withStyles(CalendarStyliser)(FcCalendarBoard)
