/*
 * File: UserStyle.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 1:15:04 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Monday, 23rd September 2019 8:51:17 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

 import Theme from '../../material/theme'

const CalendarStyliser = {
    form: {
    },
    formControl: {
    },
    input: {},
    textfield: {
        margin: '20px'
    },
    button: {
        background: 'linear-gradient(45deg, '+Theme.palette.primary.main+' 30%, '+Theme.palette.secondary.main+' 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
  }

export default CalendarStyliser;
