/*
 * File: UserStyle.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 1:15:04 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:16:55 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

const AddEventsStyliser = {
    form: {
    },
    formControl: {
    },
    checkbox: {
    },
    input: {},
    textfield: {
    },
    button: {
    },
  }

export default AddEventsStyliser;
