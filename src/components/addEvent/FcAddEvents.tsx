/*
 * File: FcAddEvent.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 12:20:25 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 8:04:23 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
import { SeaventsState } from '../../model/states/States'
import {
    MuiThemeProvider,
    FormLabel,
    Button,
    withStyles,
    Grid,
} from '@material-ui/core'
import { Events, T_Events_Props } from '../../model/Events'
import Theme from '../../material/theme'
import classNames from 'classnames';
import AddEventsStyliser from './AddEventsStyle'
import { EventFormFields, renderTextField, renderDateField, renderCheckboxField } from '../../material/FieldFactory'

interface FcAddEventsProps {
    data: SeaventsState
    classes: any
    action: {
        submitForm: (e: Events) => void
        handleChange: (key: string, value: T_Events_Props) => void
    }
}

// Stateless/presentationnal component
const FcAddEvents: React.FC<FcAddEventsProps> = (props: FcAddEventsProps) => {
    const { data, classes, action } = props;
    const newEvents = data.Events.newEvents
    const local_color = "secondary";
    const submit = (e: any) => {
        e.preventDefault();
        action.submitForm(props.data.Events.newEvents);
    }
    const variant = "outlined";
    const eventFormFields: EventFormFields[] = [
        { id:"event_title", name: "title", value: newEvents.title, label: "title", type: "text", onChange: action.handleChange, variant: variant, render: renderTextField },
        { id:"event_date", name: "date", value: newEvents.date, label: "date", type: "", onChange: action.handleChange, variant: variant, render: renderDateField },
        { id:"event_start", name: "start", value: newEvents.start, label: "start", type: "", onChange: action.handleChange, variant: variant, render: renderDateField },
        { id:"event_end", name: "end", value: newEvents.end, label: "end", type: "", onChange: action.handleChange, variant: variant, render: renderDateField },
        { id:"event_all_day", name: "allDay", value: newEvents.allDay, label: "all day", type: "checkbox", onChange: action.handleChange, variant: variant, render: renderCheckboxField },
    ];

    if (data.Events.error.code >= 300) {
        return (
          <MuiThemeProvider theme={ Theme }>
              <p>Error Code: {data.Events.error.code}</p>
              <p>Error message: {data.Events.error.message}</p>
          </MuiThemeProvider>
        );
    }
    return (
        <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                <form className={classNames(classes.form)}>
                    <FormLabel >Add en event</FormLabel>
                        { eventFormFields.map((field) => field.render(field, classes, data.Events.errors)) }
                        <Button
                            disabled={props.data.Events.errors.has}
                            variant={variant}
                            color={local_color}
                            className={classes.button}
                            onClick={submit}
                        >Add</Button>
                    </form>
                </Grid>
            </Grid>
    )
}

export default withStyles(AddEventsStyliser)(FcAddEvents)
