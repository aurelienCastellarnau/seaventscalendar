/*
 * File: AddEvent.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 12:20:10 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:15:43 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */


import * as React from 'react'
import FcAddEvents from './FcAddEvents'
import {SeaventsState} from '../../model/states/States'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'
import { Events, T_Events_Props } from '../../model/Events'
import { HandleEventsFormChange } from '../../store/actions/events/form'
import { PostEvent } from '../../store/actions/events/post'

export interface AddEventsProps  {
    data: SeaventsState,
    action: {
        submitForm: (e: Events) => void,
        handleChange: (key: string, value: T_Events_Props) => void,
    }
}

class AddEvents extends React.Component<AddEventsProps, SeaventsState> {
    render () {
        return (
            <FcAddEvents data={this.props.data} action={this.props.action}></FcAddEvents>
        )
    }
}

const mapStateToProps = (state: SeaventsState) => {
    return { data: state }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<Promise<void>, SeaventsState, any>) => {
    return {
        action: {
            submitForm: (e: Events) => {
                dispatch<any>(PostEvent(e))
            },
            handleChange: (key: string, value: T_Events_Props) => {
                dispatch<any>(HandleEventsFormChange(key, value))
            },
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddEvents)