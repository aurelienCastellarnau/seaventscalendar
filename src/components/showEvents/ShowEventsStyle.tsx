/*
 * File: UserStyle.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 1:15:04 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:20:25 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */
import { Theme as MaterialTheme } from '@material-ui/core/styles'

const ShowEventsStyliser = (Theme: MaterialTheme) => ({
    root: {
    },
    heading: {
    },
    panelselected: {
        backgroundColor: Theme.palette.secondary.main,
    },
    panel: {
        background: "#fff"
    }
  })

export default ShowEventsStyliser;
