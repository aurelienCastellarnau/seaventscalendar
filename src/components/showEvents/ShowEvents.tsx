/*
 * File: ShowEvents.tsx
 * Project: Seavents
 * File Created: Tuesday, 24th September 2019 12:51:33 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:43:20 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
import { EventsState, SeaventsState } from '../../model/states/States'
import { ThunkDispatch } from 'redux-thunk'
import { connect } from 'react-redux'
import FcShowEvents from './FcShowEvents'
import { FetchEvents } from '../../store/actions/events/get'
import { SetError } from '../../store/actions/events'
import { OptionsInput } from '@fullcalendar/core'
import { SetOptions } from '../../store/actions/calendar'

type ShowEventsProps = {
    data: EventsState
    action: {
        fetchEvents: () => void
        setError: () => void
        setOptions: (opts: OptionsInput) => void
    }
  }

export class ShowEvents extends React.Component<ShowEventsProps, EventsState> {
    render () {
        return (
            <FcShowEvents data={this.props.data} action={this.props.action}></FcShowEvents>
        )
    }
}

const mapStateToProps = (state: SeaventsState) => {
    return { data: state.Events }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<Promise<void>, EventsState, any>) => {
    return {
        action: {
            fetchEvents: () => {
                dispatch<any>(FetchEvents())
            },
            setError: () => {
                dispatch<any>(SetError({code: 0, message: ""}))
            },
            setOptions: (opts: OptionsInput) => {
                dispatch<any>(SetOptions(opts))
            }
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShowEvents)