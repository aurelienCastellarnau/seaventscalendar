/*
 * File: FcShowEvents.tsx
 * Project: Seavents
 * File Created: Tuesday, 24th September 2019 12:51:42 am
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:43:44 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import * as React from 'react'
// import PropTypes from 'prop-types'
import { EventsState } from '../../model/states/States'
import {
    Grid,
    Button,
    Typography,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    CircularProgress,
} from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ShowEventsStyliser from './ShowEventsStyle'
import CalendarBoard from '../calendar/CalendarBoard'
import AddEvents from '../addEvent/AddEvents'
import Theme from '../../material/theme'

type FcShowEventsProps = {
    data: EventsState
    classes: any
    action: {
        fetchEvents: () => void
        setError: () => void
    }
  }

// Stateless/presentationnal component
const FcShowEvents: React.FC<FcShowEventsProps> = (props: FcShowEventsProps) => {
    // injected to props by wishStyles
    const {data, classes, action} = props
    if (data.loading)  {
        return (
            <Grid container spacing={10}>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                    <CircularProgress color="secondary" />
                </Grid>
            </Grid>
        )
    } else if (data.error.code !== 0) {
        return (
            <Grid container spacing={10}>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                    <p>Error - code: {data.error.code}</p>
                    <p>message: {data.error.message}</p>
                    <Button onClick={action.setError}>Ok</Button>
                </Grid>
            </Grid>
        )
    } else {
        return (
            <Grid container spacing={1}>
                <Grid item xs={6} >
                    <Grid container spacing={1}>
                        {data.events.map((e) =>{
                            return (
                                <Grid item xs={4}
                                    key={e.id || e.title+e.dateStr} className={classes.root}>
                                    <ExpansionPanel className={e.id === data.selectedEvents.id ? classes.panelselected : classes.panel }>
                                        <ExpansionPanelSummary
                                            expandIcon={<ExpandMoreIcon />}
                                            aria-controls="panel1a-content"
                                            id="panel1a-header"
                                        >
                                            <Typography className={classes.heading}>
                                                {e.title}
                                            </Typography>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails>
                                            <ul>
                                                <li>Date: {e.date.toDateString()}</li>
                                                <li>Start: {e.start.toDateString()}</li>
                                                <li>End: {e.end.toDateString()}</li>
                                            </ul>
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Grid>
                <Grid item xs={6}>
                    <AddEvents></AddEvents>
                </Grid>
                <Grid item xs={12} sm={12} md={9} lg={8} xl={8}>
                    <CalendarBoard></CalendarBoard>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(ShowEventsStyliser(Theme))(FcShowEvents)
