/*
 * File: index.tsx
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 5:43:37 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Friday, 20th September 2019 1:19:51 am
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import thunkMiddleware from 'redux-thunk'
import  { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducer'

const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
)
export default store
