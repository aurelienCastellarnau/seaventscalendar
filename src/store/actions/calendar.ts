/*
 * File: calendar.ts
 * Project: Seavents
 * File Created: Thursday, 19th September 2019 6:30:56 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:36:04 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { ActionCreator, Action } from 'redux'
import { Events } from '../../model/Events'
import { OptionsInput } from '@fullcalendar/core'

// Calendar Actions
export const SET_OPTIONS = 'SET_OPTIONS'
export const TOGGLE_CALENDAR_WEEKENDS = "TOGGLE_CALENDAR_WEEKENDS"
export const HANDLE_DATE_CLICK = "HANDLE_DATE_CLICK"

interface payloaded {
    payload: any
}

type T_SetOptions = Action & {
    payload: OptionsInput
}

type T_ToggleCalendarWeekends = Action & payloaded

type T_HandleDateClick = Action & {
    payload: Events
}

export type CalendarActions = T_SetOptions | T_ToggleCalendarWeekends | T_HandleDateClick

export const hasPayload = (item: any): item is payloaded => {
    return item != null && 'undefined' != typeof item &&
    item.payload != typeof 'undefined'
}

export const SetOptions: ActionCreator<CalendarActions> = (payload: OptionsInput): CalendarActions => {
    return {
        type: SET_OPTIONS,
        payload: payload
    }
}

// Action creators return our possible Actions with a dynamic payload or not
export const ToggleCalendarWeekends: ActionCreator<CalendarActions> = (): CalendarActions => {
    return {
        type: TOGGLE_CALENDAR_WEEKENDS,
        payload: ""
    }
}

export const HandleDateClick: ActionCreator<CalendarActions> = (payload: Events): CalendarActions => {
    return {
        type: HANDLE_DATE_CLICK,
        payload: payload,
    }
}

