/*
 * File: post.ts
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 9:46:31 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:35:30 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { ActionCreator, Action } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { EventsActions } from './index'
import { Events } from '../../../model/Events'
import { EventsState } from '../../../model/states/States'
import { SeaventsError, isSeaventsError } from '../../../model/SeaventsError'
import { API } from '../../api/Api'

// POST an event loading/success and fail actions
export const POST_EVENTS = "POST_EVENTS"
export const POST_EVENTS_FAILURE = "POST_EVENTS_FAILURE"
export const POST_EVENTS_SUCCESS = "POST_EVENTS_SUCCESS"

export type T_PostRequest = Action & {
    payload: boolean
}

export type T_PostRequestSuccess = Action & {
    payload: Events
}

export type T_PostRequestFailure = Action & {
    payload: SeaventsError
}

export const PostEvents: ActionCreator<EventsActions> = (payload: boolean): EventsActions => {
    return {
        type: POST_EVENTS,
        payload: payload,
    }
}

export const PostEventsFailure: ActionCreator<EventsActions> = (payload: SeaventsError): EventsActions => {
    return {
        type: POST_EVENTS_FAILURE,
        payload: payload,
    }
}

export const PostEventsSuccess: ActionCreator<EventsActions> = (payload: Events): EventsActions => {
    return {
        type: POST_EVENTS_SUCCESS,
        payload: payload,
    }
}

// Async Redux action using thunk
export const PostEvent: ActionCreator<ThunkAction<Promise<void>, EventsState, void, EventsActions>> = (events: Events) => {
    // We use the redux-thunk dispatch, not the redux dispatch
    // It allow us to precise that redux must await a Promise returning void, editing our state
    // relying on our custom Actions. It return a Promise<void>...
    return async (dispatch: ThunkDispatch<Promise<void>, EventsState, EventsActions>): Promise<void> => {

        const jsonBody = JSON.stringify(events);
        console.log('PostEvent jsonBody: ', jsonBody)
        dispatch(PostEvents(true))
        const request = {url: "http://localhost:4000/events", method: "POST", jsonBody: jsonBody}
        try {
            // Es6 xhr request, we can use it because we integrate babel-polyfill
            const response = await API<Events | SeaventsError>(request.url, request.method, request.jsonBody)
            if (isSeaventsError(response)) {
                dispatch(PostEventsFailure(response))
            } else {
                dispatch(PostEventsSuccess(response))
            }
        } catch(e) {
            dispatch(PostEventsFailure(e))
        }
        dispatch(PostEvents(false))
      }
}