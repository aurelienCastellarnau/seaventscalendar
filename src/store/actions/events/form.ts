/*
 * File: form.ts
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 9:55:20 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:33:51 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */
import { ActionCreator, Action } from 'redux'
import { FormEventsActions } from './index'
import { T_Events_Props } from '../../../model/Events'

// A change in the EventsForm will trigger a key/val in action
export const HANDLE_EVENT_FORM_CHANGE = "HANDLE_EVENT_FORM_CHANGE"

export type T_HandleEventsFormChange = Action & {
    key: string
    value: T_Events_Props
}

export const HandleEventsFormChange: ActionCreator<FormEventsActions> = (key: string, value: T_Events_Props): FormEventsActions => {
    return {
        type: HANDLE_EVENT_FORM_CHANGE,
        key: key,
        value: value,
    }
}