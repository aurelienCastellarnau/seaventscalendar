/*
 * File: get.ts
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 9:52:49 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Monday, 23rd September 2019 11:31:09 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { ActionCreator, Action } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { EventsActions } from '../events'
import { Events } from '../../../model/Events'
import { EventsState } from '../../../model/states/States'
import { SeaventsError, isSeaventsError } from '../../../model/SeaventsError'
import { API } from '../../api/Api'

export const REQUEST_EVENTS = "REQUEST_EVENTS"
export const REQUEST_EVENTS_FAILURE = "REQUEST_EVENTS_FAILURE"
export const REQUEST_EVENTS_SUCCESS = "REQUEST_EVENTS_SUCCESS"
export const REQUEST_ONE_EVENTS = "REQUEST_ONE_EVENTS"
export const REQUEST_ONE_EVENTS_FAILURE = "REQUEST_ONE_EVENTS_FAILURE"
export const REQUEST_ONE_EVENTS_SUCCESS = "REQUEST_ONE_EVENTS_SUCCESS"

// During Request action, the loading will be set to true
export type T_RequestEvents = Action & {
    payload: boolean
}

// If Api call fail, the error will be transmit
export type T_RequestEventsFailure = Action & {
    payload: SeaventsError
}

// If Api call succeed, the result is transmit
export type T_RequestEventsSuccess = Action & {
    payload: Events[]
}
// Action creators return our possible Actions with a dynamic payload
export const RequestEvents: ActionCreator<EventsActions> = (payload: boolean): EventsActions => {
    return {
        type: REQUEST_EVENTS,
        payload: payload
    }
}

export const RequestEventsFailure: ActionCreator<EventsActions> = (payload: SeaventsError): EventsActions => {
    return {
        type: REQUEST_EVENTS_FAILURE,
        payload
    }
}

export const RequestEventsSuccess: ActionCreator<EventsActions> = (payload: Events[]): EventsActions => {
    return {
        type: REQUEST_EVENTS_SUCCESS,
        payload
    }
}

export const FetchEvents: ActionCreator<ThunkAction<Promise<void>, EventsState, void, EventsActions>> = () => {
    // We use the redux-thunk dispatch, not the redux dispatch
    // It allow us to precise that redux must await a Promise returning void, editing our state
    // relying on our custom Actions. It return a Promise<void>...
    return async (dispatch: ThunkDispatch<Promise<void>, EventsState, EventsActions>): Promise<void> => {

        dispatch(RequestEvents(true))
        const request = {url: "http://localhost:4000/events", method: "GET"}
        try {
            // Es6 xhr request, we can use it because we integrate babel-polyfill
            const response = await API<Events[] | SeaventsError>(request.url, request.method)
            if (isSeaventsError(response)) {
                dispatch(RequestEventsFailure(response))
            } else {
                dispatch(RequestEventsSuccess(response))
            }
        } catch(e) {
            dispatch(RequestEventsFailure(e))
        }
        dispatch(RequestEvents(false))
      }
}

// During Request action, the loading will be set to true
export type T_RequestOneEvents = Action & {
    payload: boolean
}

// If Api call fail, the error will be transmit
export type T_RequestOneEventsFailure = Action & {
    payload: SeaventsError
}

// If Api call succeed, the result is transmit
export type T_RequestOneEventsSuccess = Action & {
    payload: Events[]
}
// Action creators return our possible Actions with a dynamic payload
export const RequestOneEvents: ActionCreator<EventsActions> = (payload: boolean): EventsActions => {
    return {
        type: REQUEST_ONE_EVENTS,
        payload: payload
    }
}

export const RequestOneEventsFailure: ActionCreator<EventsActions> = (payload: SeaventsError): EventsActions => {
    return {
        type: REQUEST_ONE_EVENTS_FAILURE,
        payload
    }
}

export const RequestOneEventsSuccess: ActionCreator<EventsActions> = (payload: Events): EventsActions => {
    return {
        type: REQUEST_ONE_EVENTS_SUCCESS,
        payload
    }
}

export const FetchOneEvents: ActionCreator<ThunkAction<Promise<void>, EventsState, void, EventsActions>> = (id: any) => {
    // We use the redux-thunk dispatch, not the redux dispatch
    // It allow us to precise that redux must await a Promise returning void, editing our state
    // relying on our custom Actions. It return a Promise<void>...
    return async (dispatch: ThunkDispatch<Promise<void>, EventsState, EventsActions>): Promise<void> => {

        dispatch(RequestOneEvents(true))
        const request = {url: `http://localhost:4000/events/${id}`, method: "GET"}
        try {
            // Es6 xhr request, we can use it because we integrate babel-polyfill
            const response = await API<Events[] | SeaventsError>(request.url, request.method)
            if (isSeaventsError(response)) {
                dispatch(RequestOneEventsFailure(response))
            } else {
                dispatch(RequestOneEventsSuccess(response))
            }
        } catch(e) {
            dispatch(RequestOneEventsFailure(e))
        }
        dispatch(RequestOneEvents(false))
      }
}
