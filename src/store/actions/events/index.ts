/*
 * File: index.ts
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 9:57:55 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:34:46 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

import { ActionCreator, Action } from 'redux'
import { T_RequestEvents, T_RequestEventsFailure, T_RequestEventsSuccess } from './get'
import { T_PostRequest, T_PostRequestSuccess, T_PostRequestFailure } from './post'
import { T_HandleEventsFormChange } from './form'
import { SeaventsError } from '../../../model/SeaventsError'

// SET_ERROR action allow us to control error
// from outside an API call (reset for exemple)
export const SET_ERROR = 'SET_ERROR'

type T_SetError = Action & {
    payload: SeaventsError
}

export type EventsActions = T_RequestEvents | T_RequestEventsFailure | T_RequestEventsSuccess | T_PostRequest | T_PostRequestSuccess | T_PostRequestFailure | T_SetError
export const isEventsActions = (item: any): item is EventsActions => {
    return item !== undefined && item !== null && item.payload !== undefined
}

export type FormEventsActions = T_HandleEventsFormChange
export const isFormEventsActions = (item: any): item is FormEventsActions => {
    return item !== undefined && item !== null && item.key !== undefined && item.value !== undefined
}

export const SetError: ActionCreator<EventsActions> = (payload: SeaventsError): EventsActions => {
    return {
        type: SET_ERROR,
        payload: payload,
    }
}
