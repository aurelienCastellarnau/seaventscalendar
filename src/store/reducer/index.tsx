/*
 * File: index.tsx
 * Project: Seavents
 * File Created: Sunday, 15th September 2019 5:28:25 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:40:54 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

// react
import * as React from 'react'
import { ContainerState, EventsState, CalendarState } from '../../model/states/States'
import { combineReducers } from 'redux'
// EVENTS MANAGEMENT
import { Events, IsEventsInputCompatible } from '../../model/Events'
import { HANDLE_EVENT_FORM_CHANGE } from '../actions/events/form'
import { EventsActions, FormEventsActions, isEventsActions } from '../actions/events'
// CALENDAR
import { CalendarActions, TOGGLE_CALENDAR_WEEKENDS, HANDLE_DATE_CLICK, hasPayload, SET_OPTIONS } from '../actions/calendar'
import FullCalendar from '@fullcalendar/react'
import { Calendar, OptionsInput } from '@fullcalendar/core'
// Error and types guards
import { EventsFormError } from '../../model/EventsFormError'
import { ReduceEventsActions } from './events/eventsActions'

// initialCalendarState
const initialCalendarState: CalendarState = {
    opts: {},
    ref: React.createRef<FullCalendar>(),
    events: [],
}

// initialEventsState
const initialEventsState: EventsState = {
    loading: false,
    newEvents: new Events(),
    selectedEvents: new Events(),
    events: new Array<Events>(),
    error: {code: 0, message: "no error"},
    errors: new EventsFormError(),
}

// initialContainerState
const initialContainerState: ContainerState = {
    title: 'SEAVENTS',
}

// Reducer contain type assertion (returning previous state if bad format) and Object.assign
const calendarReducer = (state: CalendarState = initialCalendarState, action: CalendarActions) => {
    switch (action.type) {
        case SET_OPTIONS:
            return Object.assign({}, state, {
                opts: action.payload as OptionsInput,
            })
        case TOGGLE_CALENDAR_WEEKENDS:
            return Object.assign({}, state, {
                opts: Object.assign({}, state.opts, {
                    weekends: !state.opts.weekends
                })
            })
        case HANDLE_DATE_CLICK:
            if (hasPayload(action) && IsEventsInputCompatible(action.payload)) {
                const nstate: CalendarState = Object.assign({}, state, {
                    events: state.events.concat(action.payload)
                })
                // need to move
                const c: Calendar = state.ref!.current!.getApi()
                c.addEvent(action.payload)
                return nstate
            }
            console.log('Calendar Management, the item you want to add isn\'t at good format.')
            return state
        default:
            return state
    }
}

const eventsReducer = (state: EventsState = initialEventsState, action: EventsActions | FormEventsActions) => {
    if (isEventsActions(action)) {
        return Object.assign({}, state, ReduceEventsActions(action, state))
    } else {
        switch (action.type) {
            case HANDLE_EVENT_FORM_CHANGE:
                // We don't edit the state, we copy a new one with the modification, the old one still exist
                // use of val() to edit our Events
                const nEvent = Object.assign({}, state.newEvents.val(action.key, action.value));
                // a new error class will manage this edit
                const nErrors = new EventsFormError(state.errors);
                nErrors.check(action.key, action.value);
                nErrors.has = nErrors.hasError();
                return Object.assign({}, state, {
                    nEvent: nEvent,
                    errors: nErrors
                })
        default:
            return state
        }
    }
}

const containerReducer = (state: ContainerState = initialContainerState) => {
    return state
}

const rootReducer = combineReducers({
    Calendar: calendarReducer,
    Container: containerReducer,
    Events: eventsReducer,
})

export default rootReducer
