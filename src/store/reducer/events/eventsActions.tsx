/*
 * File: eventsActions.tsx
 * Project: Seavents
 * File Created: Monday, 23rd September 2019 10:05:41 pm
 * Author: Aurélien Castellarnau
 * Contact: (castellarnau.a@gmail.com)
 * -----
 * Last Modified: Tuesday, 24th September 2019 7:38:35 pm
 * Modified By: Aurélien Castellarnau
 * -----
 * Copyright © 2019 - 2019 Castellarnau Aurélien, castellarnau aurélien
 */

// react
import { EventsState } from '../../../model/states/States'
// EVENTS MANAGEMENT
import { Events, IsEvents } from '../../../model/Events'
import {
    REQUEST_EVENTS,
    REQUEST_EVENTS_SUCCESS,
    REQUEST_EVENTS_FAILURE,
    REQUEST_ONE_EVENTS,
    REQUEST_ONE_EVENTS_SUCCESS,
    REQUEST_ONE_EVENTS_FAILURE,
} from '../../actions/events/get'
import { POST_EVENTS, POST_EVENTS_SUCCESS, POST_EVENTS_FAILURE } from '../../actions/events/post'
import { EventsActions, SET_ERROR } from '../../actions/events'
// Error and types guards
import { isSeaventsError } from '../../../model/SeaventsError'
import { isBoolean } from 'util'

// Reducer contain type assertion (returning previous state if bad format) and Object.assign
export const ReduceEventsActions = (action: EventsActions, state: EventsState) => {
    switch(action.type) {
        case REQUEST_EVENTS || REQUEST_ONE_EVENTS || POST_EVENTS:
            return Object.assign({}, state, { loading: action.payload })

        case REQUEST_EVENTS_FAILURE || REQUEST_ONE_EVENTS_FAILURE || POST_EVENTS_FAILURE:
            if (isSeaventsError(action.payload)){
                return Object.assign({}, state, { error: action.payload })
            }
            console.log('Request error: cant read, bad format: ', action.payload)
            return state

        case REQUEST_EVENTS_SUCCESS:
            if (isSeaventsError(action.payload)){
                return Object.assign({}, state, { error: action.payload })
            }
            if (!isBoolean(action.payload) && !IsEvents(action.payload)) {
                let events = action.payload
                action.payload.forEach((event, index) => { events[index] = new Events(event) })
                return Object.assign({}, state, { events: events })
            }
            console.log('Request error: cant read, bad format: ', action.payload)
            return state

        case REQUEST_ONE_EVENTS_SUCCESS:
            if (isSeaventsError(action.payload)){
                return Object.assign({}, state, { error: action.payload })
            } else if (IsEvents(action.payload)) {
                return Object.assign({}, state, { selectedEvents: action.payload })
            } else {
                return state
            }

        case POST_EVENTS_SUCCESS:
            if (isSeaventsError(action.payload)) {
                return Object.assign({}, state, { error: action.payload })
            }
            if (!isBoolean(action.payload) && IsEvents(action.payload)) {
                const newEvents = new Events(action.payload)
                return Object.assign({}, state, { newEvents: newEvents })
            }
            return state

        case SET_ERROR:
                return Object.assign({}, state, {
                    error: {code:0, message: "no error"},
                })
        default:
            return state
    }
}
