import { isSeaventsError } from "../../model/SeaventsError";

/*
 * API()
 * Generic (T) and asynchronous function
 * T will be replaced by the provided type
 *
 * We can use this function by treating return throught then().catch().finally()
 * OR we can use 'await' keyword in front of this call
 */
export type APIRequest = {
    url: string,
    method: string,
    body?: any,
}

export type APIRequestGenerator = (body: any) => APIRequest

export async function API<T>(url: string, method = "GET", body?: any): Promise<T> {
    const promise = await fetch(
        url,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            method: method,
            body: body
        }
    )
    try {
        const result = await promise.json() as T;
        if (isSeaventsError(result) && result.code > 300) {
            throw result;
        }
        return result
    } catch (error) {
        // not a json, the return will fall in ErrorBoundary component
        throw error
    }
}
