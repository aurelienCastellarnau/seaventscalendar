# WEMANITY KATA

## Dépôt gitlab:

https://gitlab.com/aurelienCastellarnau/seaventscalendar

### Contenu

Ce projet est à destination d'un ami et collègue plongeur. Prestataire dans le tourisme plongée, il souhaite disposer d'un outil spécialisé pour gérer ses réservations privées.
Il s'agit ici du démarrage du client chargé de gérer ses évênements puis, plus tard, ses réservations.
Sur ce projet, les tests sont prêt mais très peu développés. Peu habitué aux tests front, la prise en main de jest/Enzyme m'a pris du temps.

Le projet est en typescript, React/Redux/Material et se découpe en 4 composants:

 - un container, composant d'entrée, chargé d'afficher le reste
 - un composant ShowEvents, chargé de récupérer les Events et de mettre à disposition le calendrier et le formulaire d'ajout
 - AddEvent et Calendar: addEvent est un formulaire (manque l'affichage de la gestion d'erreur), Calendar est une implémentation de la lib: https://fullcalendar.io/

J'ai mis l'accent sur l'intégration d'une librairie extérieure dans un workflow React/Redux. J'ai tenté de mettre l'accent sur les tests, mais j'ai finis par sacrifier un peu cette partie pour arriver à un résultat fonctionnel.

#### Arborescence:

 - components: les composants,
 - material: un model lié aux item fronts, des fonctions permettant de render facilement des éléments de formulaires, notre theme
 - model: le model front, le state. Coté client, les Events implémentent la classe EventInput requis par le fullCalendar.
 - store: l'ensemble actions/reducer de l'app

Autre:

 - snapshots: les snapshots Enzyme


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# SeaventsCalendar

Event manager React/Redux/Material thanks to https://fullcalendar.io/

## Screenshots

![vue générale: ](https://gyazo.com/9974b11811a1c74ff608b4663ee990c0)

## Prerequisites

### CLIENT

nodejs/npm

```https://nodejs.org/en/```

### Work with API:

https://gitlab.com/aurelienCastellarnau/seavents

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the non-interactive no-watch mode ).<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run testsnap`

Launches the test runner in the non-interactive no-watch mode ).<br>
Recreate Enzyme snapshots

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
